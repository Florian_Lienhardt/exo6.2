package fr.uvsq21400502;
import static org.junit.Assert.*;
import org.junit.Test;

public class FractionTest {

	@Test
		 public void TestFractionFloat()
		    {
		        Fraction f =new Fraction(10,2);
		        assertTrue("vrai",f.FractionFloat()==5);
		    }
		
	@Test
		 public void TestgetNum()
		    {
		        Fraction f =new Fraction(1,4);
		        assertTrue("vrai",f.getNum()==1);
		    }
		
	@Test
		 public void TestgetDenum()
		    {
		        Fraction f =new Fraction(1,4);
		        assertTrue("vrai",f.getDenum()==4);
		    }
	
	@Test
	public void TestTest()
	    {
			Fraction f =new Fraction(1,4);
			Fraction f2=new Fraction(3,12);
			
	        assertTrue("vrai",f.Test(f2)==true);
	    }
	}

