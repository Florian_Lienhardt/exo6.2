package fr.uvsq21400502;

public class Fraction {
	
	
		private int num;
	    private int denum;
	    
	    public Fraction(int num,int denum)
	    {
	        
	        this.num=num;
	        this.denum=denum;
	        if (denum==0)  {throw new ArithmeticException();}
	        
	    }
	    public Fraction(int numerateur)
	    {
	       this(numerateur,1);
	    }
	    
	    public Fraction()
	    {
	    	this(0,1);
	    }
	    
	    public static Fraction ZERO = new Fraction(0,1);
	    public static Fraction UN = new Fraction(1,1);
	    
	    
	    public int getNum()
	    {return num;}
	    
	    public int getDenum()
	    {return denum;}
	    
	    public double FractionFloat()
	    {
	        double res,n,d;
	        n=this.num;
	        d=this.denum;
	        res=n/d;
	        
	        return res;
	    }
	    
	    
	    public String toString()
	    {
	        return ( this.num + "/" + this.denum ) ;
	    }
	    
	    
	    public boolean Test(Fraction b)
	    {
	        double fl1=this.FractionFloat();
	        double fl2=b.FractionFloat();
	        
	        double res;
	        res= fl1-fl2;
	        
	        if(res==0)
	        {   System.out.println("Test!!!fractions égales!!!!Test");
	            return true;}
	        else {
	            System.out.println("Test!!!fractions non égales!!!Test");
	            return false;}
	    
	    
	    }
	    
	    
	    
	    public static String compare(Fraction a, Fraction b)
	    {
	        if (a.num*b.denum == b.num*a.denum)
	           { return (a.toString() + " = " + b.toString());}
	           
	        if (a.num*b.denum < b.num*a.denum)
	            {return (a.toString() + " < " + b.toString() );}
	        
	        else
	            {return (a.toString() + " > " + b.toString() );}
	    }
	    
	    public static void main(String[] args)
	    {
	    	Fraction f= new Fraction(1,3);
	    	Fraction f1= new Fraction(1,2);
	    	Fraction f2= new Fraction(1,5);
	    	//afficher num et denum
	    	System.out.println("le numerateur est"+f.getNum());
	    	System.out.println("le dénomerateur est"+f.getDenum());

	    	//afficher la fraction en double
	    	System.out.println("la fraction en flottante 1/3 donne:"+f.FractionFloat());
	    	
	    	//teste si 2 fractions sont égales
	    	boolean t=f.Test(f2);
	    	
	    	if(t)
	    	System.out.println("fractions égales ");
	    	else 
	    		System.out.println("fractions non égales");
	    	
	    	//comparaison de deux fractions
	    	System.out.println(Fraction.compare(f,f2));
	    }
	    
	    

}
